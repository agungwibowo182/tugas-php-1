<?php
require ('Animal.php');
require ('Ape.php');
require ('Frog.php');


$sheep  = new Animal("shaun");
echo "Name : ". $sheep ->get_nama()."<br>";
echo "legs : ".  $sheep ->get_legs()."<br>";
echo "cold blooded : ".  $sheep ->get_cold_blooded()."<br>";

echo "<br>";
$kodok   = new frog ("buduk");
echo "Name : ". $kodok ->get_nama()."<br>";
echo "legs : ".  $kodok ->get_legs()."<br>";
echo "cold blooded : ".  $kodok ->get_cold_blooded()."<br>"; 
echo "Jump : ".$kodok ->jump();

echo "<br>";
echo "<br>";
$sungokong = new Ape("kera sakti");
echo "Name : ". $sungokong->get_nama()."<br>";
echo "legs : ".  $sungokong->get_legs()."<br>";
echo "cold blooded : ".  $sungokong->get_cold_blooded()."<br>"; 
echo "Yell : ".$sungokong->yell();

?>